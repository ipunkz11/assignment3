import React, { Component } from 'react'
import { Text, View, TouchableOpacity, StyleSheet } from 'react-native'

export default class CButton extends Component {
    render() {
        return (
            <View>
                <TouchableOpacity {...this.props} style={{...styles.button}}>
                    <Text style={{color:'black', textAlign:'center'}}>{this.props.title}</Text>
                </TouchableOpacity>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    button: {
      marginHorizontal:10,
      borderWidth: 1,
      backgroundColor:'green',
      padding:5,
      borderRadius:10,
    }
  })