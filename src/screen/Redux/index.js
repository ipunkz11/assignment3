import React, { Component } from 'react'
import { Text, View, StyleSheet, Image, TouchableOpacity } from 'react-native'
import { connect } from 'react-redux'
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'

export class Redux extends Component {
    constructor() {
        super()
        this.state = {
            data: [],
        };
    }


    render() {
        const { inbox, navigation } = this.props
        console.log(inbox)
        return (
            <View style={styles.head}>
                <View style={{ margin: 20 }}>
                    <TouchableOpacity style={{ backgroundColor: '#2e514b', width: '10%', borderRadius: 10, borderWidth: 2, borderColor: 'pink', marginBottom: 20, padding: 5 }}>
                        <Icon name="keyboard-backspace" size={30} color="aqua" onPress={() => navigation.goBack()} />
                    </TouchableOpacity>
                    <Text style={{ fontWeight: 'bold', color: 'aqua', fontSize: 25 }}>MESSAGES</Text>
                </View>

                <View style={{ marginBottom: 50 }}>
                    {inbox.length > 0 && inbox.map((v, i) => {
                        console.log(inbox)
                        return (
                            <View key={i} style={styles.wrap}>
                                <TouchableOpacity onPress={() =>
                                    navigation.navigate('DetailMessages', {
                                        message: {
                                            title: v.title,
                                            body: v.body,
                                            image: v.image,
                                        },
                                    })
                                }>
                                    <Image source={{ uri: v.image }} style={styles.image} />
                                </TouchableOpacity>
                                <View style={styles.wrapText}>
                                    <Text style={{ fontWeight: 'bold', color: 'black' }}>{v.title}</Text>
                                    <Text style={{ color: '#1a0000' }}>{v.body}</Text>
                                </View>
                            </View>
                        )
                    })}
                </View>
            </View>
        )
    }
}

const mapStateToProps = state => {
    return {
        inbox: state.inbox,
    };
};

// const mapDispatchToProps = dispatch => {
//     return {
//         add: data => {
//             dispatch({
//                 type: 'ADD-STUDENT',
//                 payload: data,
//             });
//         },

//         delete: data => {
//             dispatch({
//                 type: 'DELETE-STUDENT',
//                 payload: data,
//             });
//         },

//         updateData: v => {
//             dispatch({
//                 type: 'UPDATE-DATA',
//                 payload: v,
//             });
//         },
//     };
// };

export default connect(mapStateToProps, null)(Redux);
const styles = StyleSheet.create({
    head: {
        paddingHorizontal: 10,
        backgroundColor: 'black',
        height: '100%'
    },
    wrap: {
        flexDirection: 'row',
        width: '90%',
        borderRadius: 15,
        borderColor: 'pink',
        borderWidth: 2,
        marginBottom: 40,
        marginLeft: 20,
        backgroundColor: '#00bfff',
        // backgroundColor: '#536872',
    },
    wrapText: {
        width: '85%',
        padding: 10,
    },
    image: {
        margin: 5,
        width: 40,
        height: 40,
        borderRadius: 40,
        borderColor: 'pink',
        borderWidth: 2,
    }
})






































// const styles = StyleSheet.create({
//     card: {
//         padding: 10,
//         borderRadius: 20,
//         borderWidth: 1,
//         margin: 5
//     },
//     tableHeader: {
//         flexDirection: 'row',
//         justifyContent: 'center',
//         alignItems: 'center',
//     },
//     number: {
//         width: '10%',
//         alignItems: 'center',
//     },
//     title: {
//         width: '30%',
//     },
// })

{/* <Text style={{ textAlign: 'center', marginVertical: 10 }}>CRUD in Redux</Text>
                <View style={styles.card}>
                    <View style={styles.tableHeader}>
                        <Text style={styles.number}>ID</Text>
                        <Text style={styles.title}>Name</Text>
                        <Text style={styles.title}>Address</Text>
                        <Text style={{ width: '20%', textAlign: 'center' }}>Action</Text>
                    </View>
                </View> */}