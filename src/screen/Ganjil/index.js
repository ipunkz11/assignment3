import React, { Component } from 'react'
import { Text, View } from 'react-native'

export default class Ganjil extends Component {
    constructor() {
        super()
        this.state = {
            data: [1, 2, 3, 4, 5, 6, 7, 8, 9, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20]
        }
    }


    render() {
        const { data } = this.state
        return (
            <View style={{flex:1, justifyContent:'center', alignItems:'center'}}>
               {data.map((v,i) => {
                   return(
                       <View key={i} >
                           <Text>
                               {v%2 != 0 && <Text>{v}</Text>}
                           </Text>
                       </View>
                   )
               })}
            </View>
        )
    }
}
