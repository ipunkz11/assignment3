import React, { Component } from 'react'
import { Text, View } from 'react-native'
import axios from 'axios'

export default class Anastasia extends Component {
    constructor() {
        super()
        this.state = {
            data: []
        }
    }

    componentDidMount() {
        const {data} = this.state
        axios
            .get('http://jsonplaceholder.typicode.com/users?id=2')
            .then(res => this.setState({ data: res.data }))
    }

    render() {
        const {data} = this.state
        console.log(data)
        return (
            <View style={{flex:1, justifyContent:'center', alignItems:'center'}}>

                {data.length > 0 && data.map((v,i) => {
                    return(
                        <View key={i}>
                            <Text>{v.website}</Text>
                        </View>
                    )
                })}
            </View>
        )
    }
}
