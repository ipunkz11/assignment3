import React, { Component } from 'react'
import { Text, View } from 'react-native'
import axios from 'axios'

export default class GetPost extends Component {
    constructor() {
        super()
        this.state = {
            data: []
        }
    }

    componentDidMount() {
        const { data } = this.state
        axios
            .get('http://jsonplaceholder.typicode.com/comments?postId=1')
            .then(res => this.setState({ data: res.data }))
    }

    render() {
        const { data } = this.state
        console.log(data)
        return (
            <View>
                {data.length > 0 && data.map((v, i) => {
                    return (
                        <View key={i}>
                            <Text>{v.id}</Text>
                            <Text>{v.name}</Text>
                            <Text>{v.email}</Text>
                            <Text>{v.body}</Text>
                        </View>
                    )
                })}
            </View>
        )
    }
}
