import React, { Component } from 'react'
import { Text, StyleSheet, View, TouchableOpacity } from 'react-native'

export default class Home extends Component {
    render() {
        const { navigation } = this.props
        return (
            <View style={{ flex: 1, justifyContent:'center', alignItems:'center', backgroundColor:'grey' }}>
                <TouchableOpacity style={{backgroundColor:'black', padding:10,borderRadius:10, width:'80%'}} onPress={() => navigation.navigate('Messages')}>
                    <Text  style={{color:'green', fontWeight:'bold', textAlign:'center'}}>MESSAGES</Text>
                </TouchableOpacity>
            </View>
        )
    }
}

