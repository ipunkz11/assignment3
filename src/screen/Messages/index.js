import React, { Component } from 'react'
import { Text, StyleSheet, View, TouchableOpacity, Image, ImageBackground } from 'react-native'
import AsyncStorageLib from '@react-native-async-storage/async-storage'
import messaging from '@react-native-firebase/messaging';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'
import Backg from '../../assets/bg.png'
// import CButton from '../../components/atoms/CButton';

export default class Messages extends Component {
  constructor() {
    super()
    this.state = {
      data: [],
      isClick: 0
    }
  }

  componentDidMount() {
    this._getData();

    messaging()
      .getToken()
      .then(Token => {
        // console.log(Token)
      })
  }

  _getData = async () => {
    try {
      const jsonValue = await AsyncStorageLib.getItem('newInboxData')
      return (
        jsonValue != null && this.setState({ data: JSON.parse(jsonValue) })
      )
    } catch (e) {
      console.log(e)
    }
  }

  _deleteData = async () => {
    await AsyncStorageLib.removeItem('newInboxData')
    this.setState({ data: [] })
  }

  _isClick = ({ isClick }) => {
    this.setState({ isClick: 1 })
  }


  render() {
    const { data, isClick } = this.state
    const { navigation } = this.props
    return (
      <View style={styles.head}>
        <ImageBackground source={Backg} resizeMode="cover">
          <View style={{ margin: 20 }}>
            <TouchableOpacity style={{ backgroundColor: '#2e514b', width: '10%', borderRadius: 10, borderWidth: 2, borderColor: 'pink', marginBottom: 20, padding: 5 }}>
              <Icon name="keyboard-backspace" size={30} color="aqua" onPress={() => navigation.goBack()} />
            </TouchableOpacity>
            <Text style={{ fontWeight: 'bold', color: 'aqua', fontSize: 25 }}>MESSAGES</Text>
            {/* <Icon name="trash-can-outline" size={40} color="#900" onPress={() => this._deleteData()} /> */}
          </View>

          <View style={{ marginBottom: 50 }}>
            {data.length > 0 && data.map((v, i) => {
              console.log(data)
              return (
                <View key={i} style={styles.wrap}>
                  <TouchableOpacity onPress={() =>
                    navigation.navigate('DetailMessages', {
                      message: {
                        title: v.title,
                        body: v.body,
                        image: v.image,
                      },
                    })
                  }>
                    <Image source={{ uri: v.image }} style={styles.image} />
                  </TouchableOpacity>
                  <View style={styles.wrapText}>
                    <Text style={{ fontWeight: 'bold', color: 'black' }}>{v.title}</Text>
                    <Text style={{ color: '#1a0000' }}>{v.body}</Text>
                  </View>
                </View>
              )
            })}
          </View>
        </ImageBackground>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  head: {
    paddingHorizontal: 10,
    backgroundColor: 'black',
    height: '100%'
  },
  wrap: {
    flexDirection: 'row',
    width: '90%',
    borderRadius: 15,
    borderColor: 'pink',
    borderWidth: 2,
    marginBottom: 40,
    marginLeft: 20,
    backgroundColor: '#00bfff',
    // backgroundColor: '#536872',
  },
  wrapText: {
    width: '85%',
    padding: 10,
  },
  image: {
    margin: 5,
    width: 40,
    height: 40,
    borderRadius: 40,
    borderColor: 'pink',
    borderWidth: 2,
  }
})
