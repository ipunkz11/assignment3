import React, { Component } from 'react'
import { Text, StyleSheet, View, TouchableOpacity } from 'react-native'

export default class Test extends Component {
    constructor() {
        super()
        this.state = {
            isClick: 0
        }
    }

    _isClick = (isClick) => {
        this.setState({isClick:1})
    }

    render() {
        const {isClick} = this.state
        console.log(isClick)
        return (
            <View style={{backgroundColor: isClick > 0 ? 'aqua' : 'green', flex:1, justifyContent:'center', alignItems:'center'}}>
                <TouchableOpacity onPress={()=> this._isClick(this.state)}>
                    <Text> Click </Text>
                </TouchableOpacity>
            </View>
        )
    }
}

const styles = StyleSheet.create({})
