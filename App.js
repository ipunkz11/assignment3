import React, { Component } from 'react'
import Route from './src/navigation'
import { Provider } from 'react-redux'
import { PersistGate } from 'redux-persist/integration/react'
import { Persistor, Store } from './src/redux/store'
import messaging from '@react-native-firebase/messaging';
import AsyncStorageLib from '@react-native-async-storage/async-storage'

messaging().onMessage(async remoteMessage => {
  Store.dispatch({
    type: 'INBOX', payload: {
      title: remoteMessage.notification.title,
      body: remoteMessage.notification.body,
      image: remoteMessage.notification.android.imageUrl,
      data: remoteMessage.data,
      isRead: 0
    }
  })
  setInboxData(remoteMessage)
});

messaging().setBackgroundMessageHandler(async remoteMessage => {
  Store.dispatch({
    type: 'INBOX', payload: {
      title: remoteMessage.notification.title,
      body: remoteMessage.notification.body,
      image: remoteMessage.notification.android.imageUrl,
      data: remoteMessage.data,
      isRead: 0
    }
  })
  setInboxData(remoteMessage)
});

const setInboxData = async (remoteMessage) => {
  try {
    let getInboxData = await AsyncStorageLib.getItem('newInboxData');
    getInboxData = JSON.parse(getInboxData);

    if (!getInboxData) {
      AsyncStorageLib.setItem('newInboxData',
        JSON.stringify(
          [
            {
              title: remoteMessage.notification.title,
              body: remoteMessage.notification.body,
              image: remoteMessage.notification.android.imageUrl,
              data: remoteMessage.data,
              isRead: 0
            }
          ]
        )
      )
    } else {
      const arrayData = [
        ...[
          {
            title: remoteMessage.notification.title,
            body: remoteMessage.notification.body,
            image: remoteMessage.notification.android.imageUrl,
            data: remoteMessage.data,
            isRead: 0
          }
        ],
        ...getInboxData
      ].slice(0, 20)
      AsyncStorageLib.setItem('newInboxData',
        JSON.stringify(arrayData)
      )
    }
  } catch (error) {
    console.log(error);
  }
}

const App = () => {
  return (
    <Provider loading={null} store={Store}>
      <PersistGate persistor={Persistor}>
        <Route />
      </PersistGate>
    </Provider>
  );
};

export default App;




























































// export default class App extends Component {
//   constructor(){
//     super();
//     this.state = {
//       data: [],
//     }
//   }

//   componentDidMount(){
//     messaging().onMessage(async remoteMessage => {
//       Alert.alert('A new FCM message arrived!', JSON.stringify(remoteMessage));
//     });
//   }

//   render() {
//     return (
//       <Provider store={Store}>
//         <PersistGate persistor={Persistor}>
//           <Route />
//         </PersistGate>
//       </Provider>
//     )
//   }
// }



